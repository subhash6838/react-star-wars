import React from 'react';
import './Input.css';

const Input = (props) => {

    let input = null;

    switch(props.elementType) {
        case ('input') :
            input =  <input {...props.elementConfig} value={props.value} onChange={props.changed} className="Input" />
        case ('textarea') :
            input =  <textarea {...props.elementConfig} value={props.value} onChange={props.changed} className="Input" />
        default :
            input =  <input {...props.elementConfig} value={props.value} onChange={props.changed} className="Input" />
    }

    return (
        <div>
            <label>{props.label}</label>
            {input}
        </div>
    )
}

export default Input;