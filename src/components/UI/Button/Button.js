import React from 'react';
import './Button.css';

const Button = (props) => {
    let classes = ["Button", props.buttonType];

    return (
        <div>
            <input type="button" onClick={props.clicked} value={props.children} className={classes.join(' ')} />
        </div>
    );
}

export default Button;