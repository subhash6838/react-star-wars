import React, { Component } from 'react';
import './Auth.css';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Auth extends Component {

    state = {
        authForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Username'
                },
                value: ''
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: ''
            }
        }
    }

    authHandler = () => {
        const authData = {};

        for (let key in this.state.authForm) {
            authData[key] = this.state.authForm[key].value;
        }

        axios.get('https://swapi.dev/api/people/?search=' + authData['name'])
            .then(response => {
                console.log(response);
                if (response.data.results[0].birth_year === authData['password']){
                    this.props.history.push("/search");
                    console.log('Successfully Login');
                }
            }).catch(error => {
                console.log(error);
            });
    }

    inputChangeHandler = (event, identifier) => {
        const authForm = {...this.state.authForm};
        const element = {...authForm[identifier]};
        element.value = event.target.value;
        authForm[identifier] = element;

        this.setState({
            ...this.status,
            authForm: authForm
        });
    }

    render () {
        const authFormElementsArray = [];

        for (let key in this.state.authForm) {
            authFormElementsArray.push({
                id: key,
                config: this.state.authForm[key]
            })
        }

        const formElements = authFormElementsArray.map(formElement => {
            return <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                changed={(event) => this.inputChangeHandler(event, formElement.id)} />
        });

        return (
            <div className="Auth">
                <form>
                    <h2>Login Form</h2>
                    {formElements}
                    <Button buttonType="success" clicked={this.authHandler}>Login</Button>
                </form>
            </div>
        );
    }
}

export default withRouter(Auth);