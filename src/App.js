import logo from './logo.svg';
import './App.css';
import Auth from './containers/Auth/Auth';
import Search from './containers/Search/Search';
import { Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Route path="/" exact render={() => <Auth />} />
      <Route path="/search" render={() => <Search />} />
    </div>
  );
}

export default App;
