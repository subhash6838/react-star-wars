import React from 'react';
import Planet from './Planet/Planet';

const Planets = (props) => {
    return (
        <div className="Planets">
            {props.planets.map((planet, index) => <Planet key={index} name={planet.name} population={planet.population} />)}
        </div>
    )
}

export default Planets;