import React, { Component } from 'react';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import './Search.css';
import axios from 'axios';
import Planets from '../../components/Planets/Planets';

class Search extends Component {

    state = {
        query: '',
        planets: []
    }

    changeInputHandler = (event) => {
        console.log('changeInputHandler called...' + event.target.value);
        this.setState({
            ...this.state,
            query: event.target.value
        });
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('prevState: ' + JSON.stringify(prevState));
        if (this.state.query && this.state.query !== prevState.query) {
            this.seachHandler();
        }
    }

    seachHandler = () => {
        console.log('clicked....' + this.state.query);
        if (this.state.query) {
            axios.get('https://swapi.dev/api/planets/?search=' + this.state.query)
                .then(response => {
                    const planets = response.data.results;
                    planets.sort(function (a, b) {
                        return a.population - b.population;
                    })
                    this.setState({
                        ...this.state,
                        planets: planets
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    render () {
        return (
            <div className="Search">
                <h2>Search Planets</h2>
                <Input elementType="input" elementConfig={{placeholder: "Search Planet..."}} value={this.state.query} changed={(event) => this.changeInputHandler(event)} />
                <Planets planets={this.state.planets} />
            </div>
        )
    }
}

export default Search;