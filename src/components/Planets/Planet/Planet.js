import React from 'react';
import './Planet.css';

const Planet = (props) => {
    return (
        <div className="Planet">
            <p>Name: {props.name}</p>
            <p>Papulation: {props.population}</p>
        </div>
    )
}

export default Planet;